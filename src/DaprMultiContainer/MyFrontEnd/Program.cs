var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDaprClient();
builder.Services.AddRazorPages();

var app = builder.Build();

// Configure the HTTP request pipeline.


app.UseHttpsRedirection();


if (!app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Error");
    app.UseHsts();
}

app.MapRazorPages();
app.Run();

